import React, { PropTypes, Component } from 'react'

export default class User extends Component{
    render(){
        const name = this.props.name;
        return <div>
            <p>1234 Привет {name}</p>
        </div>;
    }
}

User.propTypes = {
  name: PropTypes.string.isRequired
};