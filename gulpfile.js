const elixir = require('laravel-elixir');
 
require('laravel-react');
 
elixir(function(mix) {
 
    mix.elixirReactify({
        inputFile: './resources/assets/js/react/app.js', 
        inputFileName: 'bundle.js', 
        outputDirectory: 'public/js/',
        watch: './resources/assets/js/react/**'
    })
    .version(['public/js/bundle.js']);
 
});